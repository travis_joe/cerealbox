/*
 * Travis Joe
 * CS 2
 * Assignment 2.1 - Cereal Box
 *
 * Given the weight of a cereal box (ounces), output its weight in metric tons.
 * Find the number of cereal boxes which weigh 1 metric ton.
 */
#include <iostream>

using namespace std;

int main() {
    const double METRIC_TON_OUNCES = 35273.92; // Number of ounces in 1 metric ton

    double cerealBoxWeightOunces = 0; // Cereal box weight in ounces
    double cerealBoxes = 0; // Number of cereal boxes needed to equal 1 ton

    cout << "Enter the weight of a cereal box (ounces): ";
    cin >> cerealBoxWeightOunces;

    double cerealBoxWeightMetricTons = cerealBoxWeightOunces / METRIC_TON_OUNCES;
    cerealBoxes = METRIC_TON_OUNCES / cerealBoxWeightOunces;

    cout << "This cereal box is " << cerealBoxWeightMetricTons << " metric tons.\n";
    cout << cerealBoxes << " cereal boxes equals 1 metric ton.\n";
    return 0;
}